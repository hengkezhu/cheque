﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hengke;

namespace Hengke.Controllers
{
    public class ChequesController : Controller
    {
        private HengkeEntities db = new HengkeEntities();

        // GET: Cheques
        public ActionResult Index()
        {
            return View(db.Cheques.ToList());
        }

        // GET: Cheques/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheque cheque = db.Cheques.Find(id);
            if (cheque == null)
            {
                return HttpNotFound();
            }

            NumberToEnglish nte = new Hengke.NumberToEnglish();
            cheque.DollarWords =nte.changeNumericToWords(Convert.ToDouble(cheque.Dollars))+" Dollars"; //"test"+ cheque.Dollars.ToString(); 
            return View(cheque);
        }

        // GET: Cheques/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cheques/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Date,Dollars,DollarWords")] Cheque cheque)
        {
            if (ModelState.IsValid)
            {
                db.Cheques.Add(cheque);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cheque);
        }

        // GET: Cheques/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheque cheque = db.Cheques.Find(id);
            if (cheque == null)
            {
                return HttpNotFound();
            }
            return View(cheque);
        }

        // POST: Cheques/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Date,Dollars,DollarWords")] Cheque cheque)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cheque).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cheque);
        }

        // GET: Cheques/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheque cheque = db.Cheques.Find(id);
            if (cheque == null)
            {
                return HttpNotFound();
            }
            return View(cheque);
        }

        // POST: Cheques/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Cheque cheque = db.Cheques.Find(id);
            db.Cheques.Remove(cheque);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
