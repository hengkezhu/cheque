﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Hengke.Startup))]
namespace Hengke
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
