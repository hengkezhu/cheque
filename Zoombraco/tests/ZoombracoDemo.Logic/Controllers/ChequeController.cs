﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace ZoombracoDemo.Logic.Controllers
//{
//    class ChequeController
//    {
//    }
//}


// <copyright file="GenericController.cs" company="James Jackson-South">
// Copyright (c) James Jackson-South and contributors.
// Licensed under the Apache License, Version 2.0.
// </copyright>

namespace ZoombracoDemo.Logic.Controllers
{
    using System.Web.Mvc;

    using Our.Umbraco.Ditto;

    using Umbraco.Web.Models;

    using Zoombraco.ComponentModel.Caching;
    using Zoombraco.Controllers;
    using Zoombraco.Models;

    using ZoombracoDemo.Logic.Models;

    /// <summary>
    /// The page controller
    /// </summary>
    [UmbracoOutputCache]
    public class ChequeController : ZoombracoController
    {
        /// <inheritdoc />
        public override ActionResult Index(RenderModel model)
        {
            ChequeDetails chequeDetail = model.As<ChequeDetails>();
            RenderPage<ChequeDetails> viewModel = new RenderPage<ChequeDetails>(chequeDetail);

            return this.CurrentTemplate(viewModel);
        }
    }
}