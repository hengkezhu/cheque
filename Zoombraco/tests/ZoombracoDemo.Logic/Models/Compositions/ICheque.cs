﻿

namespace ZoombracoDemo.Logic.Models
{
    using System;
    using System.Collections.Generic;
    using Zoombraco.ComponentModel.PropertyValueConverters;
    using Zoombraco.Models;

    /// <summary>
    /// Encapsulates properties allowing a hero panel on the page.
    /// </summary>
    public interface ICheque
    {
        /// <summary>
        /// Gets or sets the header title
        /// </summary>
        string Names { get; set; }

        /// <summary>
        /// Gets or sets the header title
        /// </summary>
        DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the header title
        /// </summary>
        decimal Dollars { get; set; }

    }
}