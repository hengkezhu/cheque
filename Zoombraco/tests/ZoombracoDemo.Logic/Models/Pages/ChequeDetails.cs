﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoombracoDemo.Logic.Models
{
    using System.Collections.Generic;
    using Zoombraco.ComponentModel.Search;
    using Zoombraco.Models;
    /// <summary>
    /// Represents the home page document type
    /// </summary>
    public class ChequeDetails : Page, ICheque
    {
        /// <inheritdoc />
        public virtual string Names { get; set; }

        /// <inheritdoc />
        public virtual DateTime Date { get; set; }

        /// <inheritdoc />
        public virtual decimal Dollars { get; set; }
    }
}
